point location:
divide the triangulation into a regular grid
associate each cell with every triangle that intersects the cell (a triangle may be associated with multiple cells)
pick an initial random triangle within each cell as the cached triangle for that cell
when a point, q, is requested, find the cell containing the point
starting with the cached triangle, walk the triangles until arriving at the triangle containing q
 - if the cached triangle contains q, we are done
 - draw a line segment from the center(?) of the triangle to q
 - find the edge on the current triangle that intersects the line segment
 - set the current triangle to the triangle across the edge
 - repeat
replace the cached triangle with the triangle containing q
