#include <windows.h>
#include "Memory Handle/MemoryHandle.h"

#include "Memory Handle/MemoryHandle.inl"
CMemoryHandleTable oMemoryHandleTable;

#include "Common/Utilities.h"
#include "Dynamic Array/DynamicArray.h"
#include "Timer/Timer.h"
#include "Windows Input/MouseMessage.h"
#include "OpenGL 2D Circle/OpenGL2DCircle.h"
#include "Point Cloud Painter/PointCloudPainter.h"
#include "Window/Window.h"
#include "Input/Input.h"
#include "Windows Input/WindowsInputListener.h"
#include "Windows Input/WindowsInputMessage.h"
#include "Circle Bot/CircleBot.h"
#include "Obstacle Creator/ObstacleCreator.h"
#include "Window Zoom/WindowZoom.h"
#include "Color/Color.h"
#include "Constrained Delaunay Triangulation/ConstrainedDelaunayTriangulation.h"
#include "Constrained Delaunay Visualizer/ConstrainedDelaunayVisualizer.h"
#include "Constrained Delaunay Painter/ConstrainedDelaunayPainter.h"

#include "Dynamic Array/DynamicArray.inl"

int main()
{
	// Declare the message variable:
	MSG	oMessage;

	// Create the main application window and set it as the default window:
	CWindow oApplicationWindow( "Application", 0, 0, 680, 680, false );
	oApplicationWindow.Show();
	CWindow::SetDefaultWindow( &oApplicationWindow );

	// Set up window zoom on mouse wheel:
	CWindowZoom oWindowZoom;

	// Create the application window input listener:
	CInput oApplicationInput;
	CWindowsInputListener oApplicationWindowsInputListener( &oApplicationInput, oApplicationWindow );

	// Set up the frame open message:
	CMessage oFrameOpenMessage( FRAME_OPEN_MESSAGE );

	// Create the circle bot:
	CCircleBot oCircleBot( -200.f, 0.f, 200.f, 0.f );

	// Create an obstacle creator:
	CObstacleCreator oObstacleCreator;

	// Create a constrained delaunay triangulation:
	CConstrainedDelaunayTriangulation oTriangulation;
	CConstrainedDelaunayVisualizer oTriangulationVis( oTriangulation );
	CConstrainedDelaunayPainter oTriangulationPainter( oTriangulation );

	// Game loop:
	do
	{
		oApplicationWindow.SetDirty();

		// Apply force from obstacles:
		CF32Vector oForce( oObstacleCreator.CalculateForce( oCircleBot.GetPosition(), oCircleBot.GetRadius() ) );
		oCircleBot.ApplyForce( oForce );

		// Push out the frame open message which triggers updates:
		CMessenger::GlobalPush( oFrameOpenMessage );

		// Handle collision with obstacles:
		CObstacle* pCollision( oObstacleCreator.CollideObstaclesWithCircle( oCircleBot.GetPosition().X(), oCircleBot.GetPosition().Y(), 
			oCircleBot.GetRadius() ) );
		if( pCollision )
		{
			f32 fCollisionAmount( -( pCollision->GetPosition().GetDistance( oCircleBot.GetPosition() ) - pCollision->GetRadius() - 
				oCircleBot.GetRadius() ) );
			assert( fCollisionAmount > 0.f );
			oCircleBot.Collide( fCollisionAmount, pCollision->GetPosition() );
		}

		// Dispatch windows messages:
		for( u32 i( 0 ); ( i < 10 ) && PeekMessage( &oMessage, NULL, 0, 0, PM_REMOVE ); ++i )
		{
			TranslateMessage( &oMessage );
			DispatchMessage( &oMessage );
		}
	} while( !oApplicationWindowsInputListener.IsButtonDown( VK_ESCAPE ) && !oApplicationWindowsInputListener.IsButtonDown( 0 ) );

	CMessenger::GlobalStopListening( oApplicationWindowsInputListener, CWindowsInputMessage::GetWindowsInputMessageType() );
	return( (i32)oMessage.wParam );
}
